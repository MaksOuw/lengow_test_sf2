<?php

namespace App\Service;

/**
 * Class OrderConsumer
 * @package App\Service
 */
class OrderConsumer
{
    /**
     * @param string $url
     */
    public function createFromUrl(string $url)
    {
        return file_get_contents($url);
    }
}
