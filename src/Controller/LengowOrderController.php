<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderLine;
use App\Repository\CustomerRepository;
use App\Service\OrderConsumer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LengowOrderController extends AbstractController
{
    /**
     * @Route("/orders/last", name="lengow_orders_last")
     */
    public function ordersLast()
    {
        //
        // Question 1 :
        //
        // - Lister les 20 dernières commandes dont le statut est "new"
        //
        // -> Implémenter la méthode getLastNewOrders()
        // -> Utiliser le queryBuilder, trier par date et id decroissant.
        //

        $orders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->getLastNewOrders()
        ;

        return $this->render('lengow_order/new_orders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/orders/last_optimized", name="lengow_orders_last_optimized")
     */
    public function ordersLastOptimized()
    {
        //
        // Question 2 :
        //
        // - Lister les 20 dernières commandes dont le statut est "new", version optimisée
        //
        // -> Implémenter la méthode getLastNewOrdersOptimized()
        // -> Utiliser le queryBuilder, trier par date et id decroissant.
        //
        // -> La page ne doit générer qu'une seule requête SQL
        //

        $orders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->getLastNewOrdersOptimized()
        ;

        return $this->render('lengow_order/new_orders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/orders/new", name="lengow_orders_new")
     */
    public function ordersNew()
    {
        //
        // Question 3 :
        //
        // - Consommer l'API /api/orders/new et enregistrer les nouvelles commandes en base
        //
        // -> Effectuer le travail le plus simplement dans le controlleur.
        // -> Afficher le nombre total de commandes dont le statut est "new"
        //

        // Consommation de l'API
        $apiUrl = 'http://' . $this->getParameter('lengow_test_host') . $this->generateUrl('lengow_api_orders_new');
        $apiData = json_decode(file_get_contents($apiUrl));

        // Enregistrement en BDD
        $this->registerJsonData($apiData);

        // Décompte des commandes dont le statut est "new"
        $totalNewOrders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->countNewOrders()
        ;

        return $this->render('lengow_order/get_new_orders.html.twig', [
            'total_new_orders' => $totalNewOrders,
        ]);
    }

    private function registerJsonData($apiData): void
    {
        $customerRepository = $this->getDoctrine()->getRepository(Customer::class);
        $orders = [];
        foreach($apiData as $orderData)
        {
            $orders[] = $this->buildOrderFromJson($customerRepository, $orderData);
        }

        $orderRepository = $this->getDoctrine()->getManager();
        foreach($orders as $order)
        {
            $orderRepository->persist($order);
        }
        $orderRepository->flush();
    }

    private function buildOrderFromJson(CustomerRepository $repository, $orderData): ?Order
    {
        $customer = $repository->find($orderData->customer_id);
        if(! $customer instanceof Customer)
        {
            return null;
        }
        $order = new Order();
        $date = $orderData->date;
        if(! is_string($date))
        {
            $date = $orderData->date->date;
        }
        $order->setCreatedAt(new \DateTime($date));
        $order->setCustomer($customer);
        $order->setStatus(Order::STATUS_NEW);

        foreach($orderData->orderlines as $orderLines)
        {
            $orderLine = new OrderLine();
            $orderLine->setProduct($orderLines->product);
            $orderLine->setQuantity($orderLines->quantity);
            $orderLine->setPrice($orderLines->price);
            $order->addOrderLine($orderLine);
        }

        return $order;
    }

    /**
     * @Route("/orders/new_service", name="lengow_orders_new_service")
     */
    public function ordersNewService(OrderConsumer $orderConsumer)
    {
        //
        // Question 4 :
        //
        // - Consommer l'API /api/orders/new et /api/orders/new_xml et enregistrer les nouvelles commandes en base
        //   en utilisant les services.
        //
        // -> Le service prendra en paramètre l'url de l'API et devra traiter les commandes quelque soit le format de l'API (JSON et XML).
        // -> L'ajout d'un ou plusieurs autres format d'API ne devra générer aucune modification de la classe de service.
        //


        // Consommation de l'API Json
        $apiUrl = 'http://' . $this->getParameter('lengow_test_host') . $this->generateUrl('lengow_api_orders_new');
        $data = json_decode($orderConsumer->createFromUrl($apiUrl));
        $this->registerJsonData($data);

        // Consommation de l'API XML
        $apiUrl = 'http://' . $this->getParameter('lengow_test_host') . $this->generateUrl('lengow_api_orders_new_xml');
        $data = $orderConsumer->createFromUrl($apiUrl);
        $data = $this->convertXmlToJson($data);
        $this->registerJsonData($data);

        // Décompte des commandes dont le statut est "new"
        $totalNewOrders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->countNewOrders()
        ;

        return $this->render('lengow_order/get_new_orders.html.twig', [
            'total_new_orders' => $totalNewOrders,
        ]);
    }

    private function convertXmlToJson(string $data)
    {
        $xml = simplexml_load_string($data);
        $json = json_encode($xml);

        return json_decode($json);
    }

    /**
     * @Route("/orders/jquery", name="lengow_orders_jquery")
     */
    public function orderJqueryLoad()
    {
        //
        // Question 5 :
        //
        // - Consommer l'API /api/orders/random en "Ajax" pour récupérer des commandes.
        //
        // -> Utiliser jQuery pour effectuer la requête Ajax
        // -> Filtrage en javascript des résultats sur la sélection du statut.
        //

        return $this->render('lengow_order/jquery_orders.html.twig');
    }

    /**
     * @Route("/orders/vanilla_js", name="lengow_orders_vanilla_js")
     */
    public function orderVanillaJsLoad()
    {
        //
        // Question 6 :
        //
        // - Consommer l'API /api/orders/random en "Ajax" pour récupérer des commandes.
        //
        // -> Utiliser du pure javascript pour effectuer tous les traitements (chargement, affichage, filtre).
        // -> Doit être fonctionnel sous Chrome
        //

        return $this->render('lengow_order/vanilla_js_orders.html.twig');
    }
}
