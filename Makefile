USER_ID=$(shell id -u)
GROUP_ID=$(shell id -g)

export USER_ID
export GROUP_ID
export HOST_IP

docker-compose = docker-compose -f docker-compose.yml $1

.DEFAULT_GOAL := help

init: network
	chmod +x bin/*

network: ## Setup network
	$(info Network creation)
	@if [ -z "$(shell docker network ls -f name=^lengow_dev$$ -q)" ]; \
	then \
		docker network create --driver bridge lengow_dev > /dev/null; \
	fi;

up: ## Bring up application
	$(call docker-compose, up -d)

down: ## Stop application
	$(call docker-compose, down --volumes)

composer-install: ## Install composer dependencies inside container
	docker exec -ti lengow_test_sf2_test_php_1 composer install

db-init: ## Initialize database in container
	docker exec -ti lengow_test_sf2_test_php_1 bin/console doctrine:schema:create
	docker exec -ti lengow_test_sf2_test_php_1 bin/console doctrine:fixtures:load

help:
	@echo "========================================"
	@echo "Lengow/TestSf2"
	@echo "========================================"
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-$(HELP_TARGET_COLUMN_WIDTH)s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort
	@echo "========================================"


.PHONY: init network up down composer-install db-init
