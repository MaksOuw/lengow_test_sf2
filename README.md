# Test Developpeur Fullstack

#### Pré-requis & Installation 

- Serveur Web + PHP 7.2.x
- 1 Base de donnée MySQL (db_name: lengow, db_user: lengow, db_password: lengow)

Si vous êtes à l'aise avec Docker, le docker-compose est inclus dans le projet. 
Le projet est configuré pour utiliser docker, mais vous êtes libres de procéder autrement.


Une fois votre environnement de test fonctionnel, les librairies "vendor" du projet installées (via composer), toutes les instructions du test seront consultables sur la page d'accueil.

    "http://ip_ou_nom_d_hote_de_test/" 

Vous aurez peut-être besoin de cette IP ou nom d'hôte, vous pouvez renseigner la variable LENGOW_TEST_HOST du fichier d'environnement du projet (.env).

#### Notes

Aucune installation complémentaire de librairie n'est nécessaire, ni autorisée.


# Utilisation
## Dépendances
- docker >= 19.03.8
- docker-compose >= 1.25.4
- GNU/Make >= 4.2.1

## Setup docker
```bash
make init
make up
make composer-install
make db-init
```

## Acces
App : http://localhost:81  
Adminer : http://localhost:8080

# Réponses aux questions
La plupart des questions sont répondues directement via le code.  

2 - Rajouter du cache permettrait d'optimiser les temps de chargements et éviterait d'avoir à requêter constamment la base de données.

**Pour les questions 3 et 4, si les requêtes exécutées ne fonctionnent pas, il faut changer la variable `LENGOW_HOST_TEST` du `.env` pour mettre le même nom que le conteneur `nginx`.**
